﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using XTest.Abs.Enums;

namespace XTest.Abs.Entity
{
    public interface IQuestionEntity
    {
        ITranslationEntity Description { get; set; }
        string Answer { get; set; }
        QuestionType QuestionType { get; set; }

    }
}
