﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace XTest.Abs.Entity.Common.Config
{
    public class TranslationConfig
    {
        public string Language { get; set; }
        public string Value { get; set; }
    }
}
