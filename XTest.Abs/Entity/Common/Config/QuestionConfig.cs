﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace XTest.Abs.Entity.Common.Config
{
   public class QuestionConfig
    {
        public List<TranslationConfig> Name { get; set; }

        public int Count { get; set; }
    }
}
