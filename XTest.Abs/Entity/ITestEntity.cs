﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using XTest.Abs.Enums;

namespace XTest.Abs.Entity
{
    public interface ITestEntity<T> : IBaseTestEntity
    {
        ITranslationEntity Teoty { get; set; }
        CodingType CodingType { get; set; }
        List<T> Questions { get; set; }

    }
}
