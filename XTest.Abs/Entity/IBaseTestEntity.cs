﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using XTest.Abs.Enums;

namespace XTest.Abs.Entity
{
    public interface IBaseTestEntity
    {
      //  Guid IdGuid { get; set; }
        TestType TestType { get; set; }
        ITranslationEntity Name { get; set; }
    }
}
