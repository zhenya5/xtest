﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using XTest.Abs.Entity;

namespace XTest.Abs.Proсess
{
    public abstract class AbstractFactory
    {
        public abstract ITestEntity<IQuestionEntity> CreateFayraCode();
        public abstract ITestEntity<IQuestionEntity> CreateCodeBCH();
    }
}
