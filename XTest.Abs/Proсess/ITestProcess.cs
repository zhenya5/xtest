﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using XTest.Abs.Entity;
using XTest.Abs.Entity.Common.Results;

namespace XTest.Abs.Proсess
{
    public interface ITestProcess
    {
        IDataResult<IBaseTestEntity> GetTest();//get config

        IDataResult<ITestEntity<IQuestionEntity>> GetQuestions(string nameTest);
    }
}
